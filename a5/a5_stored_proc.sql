use eea19;

IF OBJECT_ID(N'dbo.customer_balance', N'P') IS NOT NULL
DROP PROC dbo.customer_balance
GO

CREATE PROC dbo.customer_balance
    @per_lname_p varchar(30)
AS
BEGIN
    select p.per_id, per_fname, per_lname, i.inv_id,
        FORMAT(sum(pay_amt), 'C', 'en-us') as total_paid,
        FORMAT((inv_total - sum(pay_amt)), 'C', 'en-us') as invoice_diff
        from person p
            join dbo.customer c on p.per_id=c.per_id
            join dbo.contact ct on c.per_id=ct.per_cid
            join dbo.[order] o on ct.cnt_id=o.cnt_id
            join dbo.invoice i on o.ord_id=i.ord_id
            join dbo.payment pt on i.inv_id=pt.inv_id
            -- must be contained in group by, if not used in aggregate function
        where per_lname=@per_lname_p
        group by p.per_id, i.inv_id, per_lname, per_fname, inv_total;
END
GO

-- can initialize variable to empty string, also case-insensitive
DECLARE @per_lname_v varchar(30) = 'halpert';

-- call stored procedure
exec dbo.customer_balance @per_lname_v;

select * from information_schema.routines
where routine_type = 'PROCEDURE';
go

IF OBJECT_ID(N'dbo.product_drill_down', N'P') IS NOT NULL
DROP PROC dbo.product_drill_down;
GO

CREATE PROC dbo.product_drill_down AS
BEGIN
select pro_name, pro_qoh,
FORMAT(pro_cost,'C', 'en-us') as cost,
FORMAT(pro_price,'C', 'en-us') as price,
str_name, cty_name, ste_name, reg_name
from product p
join sale s on p.pro_id=s.pro_id
join store sr on sr.str_id=s.str_id
join city c on sr.cty_id=c.cty_id
join state st on c.ste_id=st.ste_id
join region r on st.reg_id=r.reg_id
order by pro_qoh desc;
END
GO

-- call procedure
exec dbo.product_drill_down;