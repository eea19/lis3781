# LIS3781 Advanced Database Management

## Elka Anistratenko

### Assignment 5 Requirements:
1. Creating a high volume database using MS SQL Server.
    - adding additional table to database from previous assignment.
2. Salting and Hashing data using SHA 512.
3. Use MS SQL Server to create ERD.

| Home Office Supply Company ERD |
| -- |
| ![A5 ERD](img/a5_erd.png) |

| SQL Statement #2 |
| -- |
| ![SQL Statement #2](img/a5_stored_proc.png) |