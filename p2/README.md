> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Elka Anistratenko

### Project 2 Requirements:

*Three Parts:*

1. Download MongoDB and import json data set.
2. Screenshot MongoDB shell commands and 1 required report/JSON code solution.
3. Learning about NoSQL.

#### Project 2 Screenshots:

MongoDB shell commands: | Required report/JSON code solution: 
--- | --- 
![MongoDB shell commands](img/mongo_1.png) | ![Required report/JSON code solution](img/mongo_2.png) 