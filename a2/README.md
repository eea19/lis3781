> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Extensible Enterprise Solutions

## Elka Anistratenko

### Assignment 2 Requirements:

*Four Parts*:

1. Write and run lis3781_a2_solutions.sql code locally and on CCI server.
2. Create 2 new users with access to localhost and grant privileges.
    - Limit user1 to select, update, and delete privileges on company and customer tables.
    - Limit user2 to select, and insert privileges on customer table.
3. Chapter 11 Questions
4. Link to Bitbucket repo

#### README.md file should include the following items:

* Screenshots of lis3781_a2_solutions.sql code.
* Screenshots of insert statements and query result sets.
* Screenshots of grants for admin, user1, and user2.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

*Screenshots of lis3781_a2_solutions.sql code*:

Company Table: | Customer Table:
--- | ---
![Company Table lis3781_a2_solutions.sql](img/a2_company_table.png) | ![Customer Table lis3781_a2_solutions.sql](img/a2_customer_table.png)


##### Granting Privileges Screenshots:

*Show All Data in Tables*:
![A2 Tables](img/a2_tables.png)


*Show Grants*:

Admin: | User1: | User2:
--- | --- | ---
![Admin Grants](img/admin_grants.png) | ![User1 Grants](img/user1_grants.png) | ![User2 Grants](img/user2_grants.png)


*Display Current User/Version and List Tables*

Display Current User and Version (as user2): | List Tables (as admin):
--- | ---
![Current User and Version](img/user2_user_version.png) | ![List Tables](img/show_tables.png)


*Display Table Structures*:

![Describe Tables](img/describe_tables.png)


*Display Data for Both Tables*:

Company Table (as user2): | Customer Table (as user1)
--- | ---
![Company Table (user2)](img/user2_select_company_denied.png) | ![Customer Table (user1)](img/user1_select_customer.png)


*INSERT INTO company and customer tables as user1*:

![Insert denied on user1](img/user1_inserts_denied.png)


*SELECT from company table and DELETE from customer table as user2*:

![Select and delete denied on user2](img/user2_select_delete_denied.png)


*DROP tables as admin*:
![DROP Tables](img/drop_tables.png)