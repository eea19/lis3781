-- set foreign_key_checks=0

drop database if exists eea19;
create database if not exists eea19;
use eea19;

-- Table company

DROP TABLE if exists company;
create table if not exists company
(
cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_type enum('C-Corp', 'S-Corp', 'Non-Profit-Corp','LLC', 'Partnership'),
cmp_street VARCHAR(30) NOT NULL,
cmp_city VARCHAR(30) NOT NULL,
cmp_state CHAR(2) NOT NULL,
cmp_zip char(9) NOT null,
cmp_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,345,678.90',
cmp_email VARCHAR(100) NULL,
cmp_url VARCHAR(100) NULL,
cmp_notes VARCHAR(255) NULL,
PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES

(null, 'C-Corp','123 city dr', 'orlando', 'FL', '321431234', '1029384756', '123456.2', 'person1@gmail.com',null, 'notes'),
(null, 'S-Corp', '321 main street','tampa', 'FL','312432345', '9087654321', '123859.3', 'person2@gmail.com', 'http://www.person2.com', 'notes'),
(null, 'Non-Profit-Corp','452 street dr','miami', 'FL','353213456', '3849283094' ,'5749203.4', 'person3@gmail.com',null,null),
(null, 'LLC', '4328 drive st.','tampa', 'FL','336534567', '1234568908', '432859.3', null, 'http://www.person4.com',null),
(null, 'Partnership','4382 street circle','miami', 'FL','312455678', '3482930495' ,'5749.2', 'person5@gmail.com',null, 'notes');

SHOW WARNINGS;

-- Table customer

Drop Table if exists customer;
create table if not exists customer
(
cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_id INT UNSIGNED NOT NULL,
cus_ssn binary(64) NOT NULL,
cus_salt binary(64) NOT NULL COMMENT '*only* demo purposes - do NOT use salt as real name!' ,
cus_type enum('Loyal', 'Discount', 'Impulse', 'Need-Based', 'Wandering'),
cus_first VARCHAR(15) NOT NULL,
cus_last VARCHAR(30) NOT NULL,
cus_street VARCHAR(30) NOT NULL,
cus_city VARCHAR(30) NOT NULL,
cus_state CHAR(2) NOT NULL,
cus_zip CHAR(9) NOT NULL, 
cus_phone bigint unsigned NOT NULL COMMENT 'snn and zip codes can be zero filled, but not US area codes',
cus_email VARCHAR(100) NULL,
cus_balance DECIMAL(7,2) unsigned NULL COMMENT '12,345.67',
cus_tot_sales DECIMAL(7,2) unsigned NULL,
cus_notes VARCHAR(255) NULL,
PRIMARY KEY(cus_id),

UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
INDEX idx_cmp_id (cmp_id ASC),

CONSTRAINT fk_customer_company
  FOREIGN KEY (cmp_id)
  REFERENCES company (cmp_id)
  ON DELETE NO ACTION
  ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

-- salting and hashing sensitive data (i.e., ssn). Normally, *each* record would receive unique random salt!
set @salt=RANDOM_BYTES(64);

INSERT INTO customer
values
(null,2, unhex(SHA2(CONCAT(@salt, 123456789),512)),@salt, 'Loyal', 'Michael', 'Scott','1486 Square Circle', 'Tampa', 'FL', 
'336261234', '8084445637', 'person1@gmail.com' ,'1234.23','12345.43', 'notes'),
(null,4, unhex(SHA2(CONCAT(@salt, 987654321),512)),@salt, 'Discount', 'Dwight', 'Schrute','2901 Roundabout Way', 'Tallahassee', 'FL',
'324042345', '5057772835', 'person2@gmail.com','32124.23','432.54',null),
(null,5, unhex(SHA2(CONCAT(@salt, 192837465),512)),@salt, 'Impulse', 'Jim', 'Halpert','351 Drive St.', 'Orlando', 'FL', 
'324112347', '2026668884', 'person3@gmail.com' ,'432.12','23412.43', 'notes'),
(null,3, unhex(SHA2(CONCAT(@salt, 918273645),512)),@salt, 'Need-Based', 'Meredith', 'Palmer','200 Turnabout Dr', 'Miami', 'FL', 
'338123419', '6068176584', 'person4@gmail.com' ,'324.12','234.43',null),
(null,1, unhex(SHA2(CONCAT(@salt, 135792468),512)),@salt, 'Wandering', 'Phyllis', 'Vance', '3111 Street Drive', 'Daytona', 'FL', 
'324912342', '2840082090', 'person5@gmail.com' ,'453.12','1234.54', 'notes');

SHOW WARNINGS;
select * from company;
select * from customer;

