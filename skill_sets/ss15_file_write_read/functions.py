#!/usr/bin/env python3

# Developer: Elka Anistratenko
# Course: LIS4369
# Semester: Fall 2021

# import for reading current directory

import os

def get_requirements():
    print("\nPython Read/Write File")
    print("Developer: Elka Anistratenko")
    print("\nProgram Requirements:\n")
    print("1. Create write_read_file subdirectory with two files: main.py and functions.py.")
    print("2. Use President Abraham Lincoln's Gettysburg Address: Full Text.")
    print("3. Write address to file.")
    print("4. Read address from same file.")
    print("5. Create Python Docstrings for functions in functions.py file.")
    print("6. Display Python Docstrings for each function in functions.py file.")
    print("7. Display full file path.")
    print("8. Replicate display below.")

    print("\nHelp on function write_read_file in module functions: ")
    print("\nwrite_read_file()")
    print("   Usage: Calls two functions:")
    print("      1. file_write() # writes to file")
    print("      2. file_read() # reads from file")
    print("   Parameters: none")
    print("   Returns: none")
    print("\nHelp on function file_write in module functions:")
    print("\nfile_write()")
    print("   Usage: creates file, and writes contents of global variable to file")
    print("   Parameters: none")
    print("   Returns: none")
    print("\nHelp on function file_read in module functions:")
    print("\nfile_read()")
    print("   Usage: reads contents of written file")
    print("   Parameters: none")
    print("   Returns: none\n")


def file_read():
    print("\nPresident Abraham Lincoln's Gettysburg Address:")
    f = open("test.txt", "r")
    print(f.read())
    print("Full File Path: ")
    print(os.path.realpath(f.name))
