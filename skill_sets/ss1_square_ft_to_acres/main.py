#!/usr/bin/env python3
# Developer: Elka Anistratenko
# Course: LIS4369
# Semester: Fall 2021

import functions as f

def main():
    f.get_requirements()
    # f.user_input()
    f.calculate_sqft_to_acre()
if __name__ == "__main__":
    main()