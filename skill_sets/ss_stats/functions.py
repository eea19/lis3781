#!/usr/bin/env python3

# Developer: Elka Anistratenko
# Course: LIS4369
# Semester: Fall 2021

def get_requirements():
	print("Python Simple Statistics")
    print("\nProgram Requirements:\n"
        + "1. Create stats subdirectory with two files: main.py and functions.py.\n"
        + "2. Create integer list (see below).\n"
        + "3. Create user-defined get_range() function.\n"
        + "4. Use Python intrinsic (built-in) functions from \"statistics\" module.\n")

def get_range(my_list):
    

def get_stats():
    # initialize variables
    temperature = 0.0
    choice = ' '
    type = ' '

    # IPO: Input > Process > Output
    # get user data
    print("Input: ")

    choice = input("Do you want to convert a temperature (y or n)? ").lower()
    
    # Process and Output:
    print("\nOutput: ")

    while (choice[0] == 'y'):
        type = input("Fahrenheit to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"c\": ").lower()

        if type[0] == 'f':
            temperature = float(input("Enter temperature in Fahrenheit: "))
            temperature = ((temperature - 32)*5)/9
            print("Temperature in Celsius = " + str(temperature))
            choice = input("\nDo you want to convert another temperature (y or n)? ").lower()

        elif type[0] == 'c':
            temperature = float(input("Enter temperature in Celsius: "))
            temperature = (temperature * 9/5) + 32
            print("Temperature in Fahrenheit = " + str(temperature))
            choice = input("\nDo you want to convert another temperature (y or n)? ").lower()

        else:
            print("Incorrect entry.Please try again.")
            choice = input("\nDo you want to convert another temperature (y or n)? ").lower()
    
    print("Thank you for using our Temperature Conversion Program!")