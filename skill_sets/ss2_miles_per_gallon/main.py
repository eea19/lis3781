#!/usr/bin/env python3
# Developer: Elka Anistratenko
# Course: LIS4369
# Semester: Fall 2021

from functions import *

def main():
    print_requirements()
    calc_mpg()

if __name__ == "__main__":
    main()
