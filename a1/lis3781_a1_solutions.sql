select emp_id, emp_fname, emp_lname,
    CONCAT(emp_street, ",", emp_city, ",", emp_state, " ",substring(emp_zip,1,5), '-', substring(emp_zip,6,4)) as address,
    CONCAT('(', substring(emp_phone,1,3), ')', substring(emp_phone,4,3), '-', substring(emp_phone,7,4)) as phone_num,
    CONCAT(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2), '-', substring(emp_ssn,6,4)) as emp_ssn, job_title
from job as j, employee as e
where j.job_id = e.job_id
order by emp_lname desc;



select e.emp_id, emp_fname, emp_lname, eht_date, eht_job_id, job_title, eht_emp_salary, eht_notes
from employee e, emp_hist h, job j
where e.emp_id = h.emp_id
and eht_job_id = j.job_id
order by emp_id, eht_date;



select emp_fname, emp_lname, emp_dob,
TIMESTAMPDIFF(year, emp_dob, curdate()) as emp_age,
dep_fname, dep_lname, dep_relation, dep_dob,
TIMESTAMPDIFF(year, dep_dob, curdate()) as dep_age
from employee
natural join dependent
order by emp_lname;



start transaction;
select * from job;
update job
set job_title='owner'
where job_id=1;
select * from job;
commit;

drop procedure if exists insert_benefit;
delimiter //
create procedure insert_benefit()
begin
select * from benefit;
insert into benefit
(ben_name, ben_notes)
values 
('new benefit', 'testing');
select * from benefit;
end //
delimiter ;
call insert_benefit();



select emp_id, emp_lname, emp_fname, emp_ssn, emp_email, dep_lname, dep_fname, dep_ssn, dep_street, emp_city, dep_state, dep_zip, dep_phone
from employee
natural left outer join dependent
order by emp_lname;

select emp_id,
concat(emp_lname,",", emp_fname) as employee,
concat(substring(emp_ssn,1,3), '-', substring(emp_ssn,4,2),'-', substring(emp_ssn,6,4)) as emp_ssn,
emp_email as email,

concat(dep_lname,",", dep_fname) as dependent,
concat(substring(dep_ssn,1,3), '-', substring(dep_ssn,4,2),'-', substring(dep_ssn,6,4)) as dep_ssn,
concat(dep_street, ",", emp_city, ",", dep_state, " ", substring(dep_zip,1,5), '-', substring(dep_zip,6,4)) as address,
concat('(', substring(dep_phone,1,3), ')', substring(dep_phone,4,3), '-', substring(dep_phone,7,4)) as phone_num

from employee
natural left outer join dependent
order by emp_lname;



drop trigger if exists trg_employee_after_insert;
delimiter //
create trigger trg_employee_after_insert
after insert on employee
for each row
begin
insert into emp_hist
(emp_id, eht_date, eht_type, eht_job_id, eht_emp_salary, eht_usr_changed, eht_reason, eht_notes)
values 
(NEW.emp_id, now(), 'i', NEW.job_id, NEW.emp_salary, user(), "new employee", NEW.emp_notes);
end //
delimiter ;

insert into employee (job_id, emp_fname, emp_lname, emp_street, emp_city, emp_state, emp_zip, emp_phone, emp_email, emp_ssn, emp_dob, emp_start_date, emp_end_date, emp_salary, emp_notes)
values
(2, 'Hila', 'Klein', '957 Youtube St.', 'Los Angeles', 'CA', 888267456, 2974447507, 'h3@gmail.com', 444962805, '1985-08-04', '2005-05-18', '2015-04-16', 5647.86, 'test trigger');

select * from employee;
select * from emp_hist;