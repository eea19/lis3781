> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Elka Anistratenko

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation with mySQL running
3. Questions
4. Entity Relationship Diagram, and SQL Queries 1-7
5. Bitbucket repo links:
   a) this assignment
   b) the completed tutorial (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of A1 ERD
* SQL solutions 1-7
* git commands w/ short descriptions

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

> ### A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories must be tracked. Also, include the following business rules:
 
• Each employee may have one or more dependents. 

• Each employee has only one job.  

• Each job can be held by many employees. 

• Many employees may receive many benefits. 

• Many benefits may be selected by many employees (though, while they may not select any benefits — any dependents of employees may be on an employee’s plan). 

Notes:  

• Employee/Dependent tables must use suitable attributes (See Assignment Guidelines); 

In Addition: 

• Employee: SSN, DOB, start/end dates, salary; 

• Dependent: same information as their associated employee (though, not start/end dates), date added (as dependent), type of relationship: e.g., father, mother, etc. 
 
• Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.) 

• Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.) 

• Plan: type (single, spouse, family), cost, election date (plans must be unique) 

• Employee history: jobs, salaries, and benefit changes, as well as who made the change and why; 

• Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ); 

• *All* tables must include notes attribute. 


#### Assignment Screenshots:

| *Screenshot of AMPPS running* | *Screenshot of MySQL running in terminal* |
| --- | --- |
| ![AMPPS Installation Screenshot](img/ampps.png) | ![AMPPS MySQL Installation Screenshot](img/mysql_running.png) |

*Screenshot of A1 Entity-Relationship Diagram*:

![A1 ERD Screenshot](img/a1_ERD.png)

### SQL Queries Ex. 1-7 Output

#### Query Ex. 1
![Query 1](img/query_1.png)

#### Query Ex. 2
![Query 2](img/query_2.png)

#### Query Ex. 3
![Query 3](img/query_3.png)

|  Query Ex. 4 |  Query Ex. 5 |
| ---------- | ---------- |
| ![Query 4](img/query_4.png) | ![Query 5](img/query_5.png) |

#### Query Ex. 6
![Query 6](img/query_6.png)

#### Query Ex. 7
![Query 7](img/query_7.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/eea19/bitbucketstationlocations/ "Bitbucket Station Locations")
