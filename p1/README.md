> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Elka Anistratenko

### Project 1 Requirements:

*Three Parts*:

1. Use MySQL to create assignment/court database.
2. Use SHA2 to protect sensitive information.
3. Create stored procedures to salt and hash.

#### README.md file should include the following items:

* Screenshot of ERD.
* Screenshot of populated person table.
* Links to p1.mwb and p1.sql files.

#### ERD:

![Project 1 ERD](img/p1_erd.png)

#### Person Table:

![Person Table](img/person.png)

#### Links to p1.mwb and p1.sql files:

[p1.mwb file](https://bitbucket.org/eea19/lis3781/src/master/p1/p1.mwb)

[p1.sql file](https://bitbucket.org/eea19/lis3781/src/master/p1/lis3781_p1_solutions.sql)