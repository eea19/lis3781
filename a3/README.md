> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Extensible Enterprise Solutions

## Elka Anistratenko

### Assignment 3 Requirements:

*Three Parts*:

1. Download Microsoft Remote Desktop in order to operate Oracle SQL Developer.
2. Write lis3781_a3_solutions.sql code to create tables and data.
3. Run lis3781_a3_solutions.sql code using oracle on Microsoft Remote Desktop.

#### README.md file should include the following items:

* Screenshots of lis3781_a3_solutions.sql insert statements/table creation.
* Screenshots of lis3781_a3_solutions.sql generated tables in Oracle environment.
* Screenshots of SQL query examples.

#### Customer Table:

| Customer Table Creation: | Customer Insert Statements:
| --- | --- |
|![Customer Table Creation](img/cus_creation.png) |![Customer Table Inserts](img/cus_inserts.png) |

| Customer Output |
| --- |
|![Customer Output](img/cus_output.png) |

#### Commodity Table:

| Commodity Table Creation: | Commodity Insert Statements:
| --- | --- |
|![Commodity Table Creation](img/com_creation.png) |![Commodity Table Inserts](img/com_inserts.png) |

| Commodity Output |
| --- |
|![Commodity Output](img/com_output.png) |

#### Order Table:

| Order Table Creation: | Order Insert Statements:
| --- | --- |
|![Order Table Creation](img/order_creation.png) |![Order Table Inserts](img/order_inserts.png) |

| Commodity Output |
| --- |
|![Order Output](img/order_output.png) |

#### Example SQL Queries:

![SQL Queries 1](img/queries_1.png)
![SQL Queries 2](img/queries_2.png)
![SQL Queries 3](img/queries_3.png)
![SQL Queries 4](img/queries_4.png)