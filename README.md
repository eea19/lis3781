> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Elka Anistratenko

### LIS3781 Requirements:

*Course Assignments*:

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Provide screenshots of AMPPS/mySQL installations
    - Create lis3781 Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Write and run lis3781_a2_solutions.sql code locally and on CCI server, and provide screenshots.
    - Create 2 new users with access localhost and grant privileges, and provide screenshots.
        - Limit user1 to select, update, and delete privileges on company and customer tables.
        - Limit user2 to select, and insert privileges on customer table.
    - Chapter 11 Questions

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Write lis3781_a3_solutions.sql code
        - Provide screenshots of created tables and insert statements
    - Access Oracle SQL Server
        - Download Microsoft Remote Desktop if necessary (Mac users)
        - Provide screenshots of lis3781_a3_solutions.sql output in Oracle
    - Provide screenshots of SQL queries (optional)
    - Chapter 12 Questions

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Creating a high volume database using MS SQL Server.
    - Salting and Hashing data using SHA 512.
    - Use MS SQL Server to create ERD.
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Creating a high volume database using MS SQL Server.
        - adding additional table to databse from previous assignment.
    - Salting and Hashing data using SHA 512.
    - Use MS SQL Server to create ERD.

*Course Projects*:

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Use MySQL to create assignment/court database.
        - Provide screenshots of ERD and populated person table.
    - Use SHA2 to protect sensitive information.
    - Create stored procedures to salt and hash.
    - Link to p1.mwb and p1.sql files.

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Download MongoDB and import json data set.
    - Screenshots of MongoDB shell commands and 1 required report/JSON code solution.
    - Learning about NoSQL.