# LIS3781 Advanced Database Management

## Elka Anistratenko

### Assignment 4 Requirements:
1. Creating a high volume database using MS SQL Server.
2. Salting and Hashing data using SHA 512.
3. Use MS SQL Server to create ERD.

| Home Office Supply Company ERD |
| -- |
| ![A4 ERD](img/a4_erd.png) |